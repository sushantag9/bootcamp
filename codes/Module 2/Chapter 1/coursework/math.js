function sin1() {
    let t11 = document.getElementById("t11");
    let t12 = document.getElementById("t12");
    var a = Math.PI / 180 * parseFloat(t11.value);
    var b = Math.sin(a);
    t12.value = b.toString();
}
function cos1() {
    let t21 = document.getElementById("t21");
    let t22 = document.getElementById("t22");
    var c = Math.PI / 180 * parseFloat(t21.value);
    var d = Math.cos(c);
    t22.value = d.toString();
}
//# sourceMappingURL=math.js.map
# This is going to be assignments folder 

Module 2: Chapter 3 Decision Making
----
### Assignments

1. Check whether the entered number is even or odd.

2. Enter the three sides of a triangle, find its type: isosceles, equilateral or scalene triangle & also check whether it is a right angled triangle.

3. Find the real and imaginary parts of a complex number.Along with the sign of real & imaginary number.

4. Enter the coordinates of the vertices of the triangle. <br>Enter the coordinates of a point & check whether it lies inside or outside the
triangle. <br>
Take the reference from the below image for this assignment

![ref](ref.jpeg.png)


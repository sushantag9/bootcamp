function upper() {
    var r1 = document.getElementById("string");
    var p1 = document.getElementById("uppercase");
    var values = r1.value;
    var substring = values.toUpperCase();
    console.log(values);
    p1.innerHTML = "String converted to Upper Case is:" + substring;
}
function lower() {
    var r1 = document.getElementById("string");
    var p2 = document.getElementById("lowercase");
    var values = r1.value;
    var substring = values.toLowerCase();
    console.log(values);
    p2.innerHTML = "String converted to Lower Case is:" + substring.toString();
}
function split() {
    var r1 = document.getElementById("string");
    var p2 = document.getElementById("splitwords");
    var values = r1.value;
    var array = values.split(" "); // Split " " space as a delimeter
    console.log(values);
    p2.innerHTML = "after split : ";
    for (let index = 0; index < array.length; index++) {
        p2.innerHTML += "</br>" + array[index];
    }
}
//# sourceMappingURL=strings2.js.map
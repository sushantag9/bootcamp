var m1 = document.getElementById("m1");
var m2 = document.getElementById("m2");
var a1 = document.getElementById("a1");
var a2 = document.getElementById("a2");
var x = document.getElementById("x");
var y = document.getElementById("y");
var rmag = document.getElementById("rmag");
var rang = document.getElementById("rang");
var r1 = document.getElementById("r1");
function calc() {
    var a = Math.PI / 180 * parseFloat(a1.value);
    var d1 = parseFloat(m1.value) * Math.sin(a);
    var aa = Math.PI / 180 * parseFloat(a2.value);
    var dd1 = parseFloat(m2.value) * Math.sin(aa);
    var fy = d1 + dd1;
    y.value = fy.toString();
    var a = Math.PI / 180 * parseFloat(a1.value);
    var d2 = parseFloat(m1.value) * Math.cos(a);
    var aa = Math.PI / 180 * parseFloat(a2.value);
    var dd2 = parseFloat(m2.value) * Math.cos(aa);
    var fx = d2 + dd2;
    x.value = fx.toString();
    rmag.value = (Math.sqrt(Math.pow(fx, 2) + Math.pow(fy, 2))).toString();
    rang.value = ((Math.atan2(fy, fx)) * (180 / Math.PI)).toString();
}
//# sourceMappingURL=app.js.map
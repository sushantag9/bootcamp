# Welcome to MathJax
![Vlabs Logo](mj-logo.svg)
## A JavaScript display engine for mathematics that works in all browsers.
## No more setup for readers. It just works.


***Features***
- High-quality typography
- Modular Input & Output
- Accessible & reusable

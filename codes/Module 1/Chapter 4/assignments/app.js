var n1 = document.getElementById("n1");
var n2 = document.getElementById("n2");
var r1 = document.getElementById("r1");
function check() {
    if (n1.value.length < 1) {
        r1.value = "Error in Num 1";
    }
    else if (n2.value.length < 1) {
        r1.value = "Error in Num 2";
    }
    else {
        return true;
    }
}
function add() {
    r1.value = "";
    if (check()) {
        var c = parseFloat(n1.value) + parseFloat(n2.value);
        r1.value = c.toString();
    }
}
function sub() {
    r1.value = "";
    if (check()) {
        var d = parseFloat(n1.value) - parseFloat(n2.value);
        r1.value = d.toString();
    }
}
function mul() {
    r1.value = "";
    if (check()) {
        var d = parseFloat(n1.value) * parseFloat(n2.value);
        r1.value = d.toString();
    }
}
function div() {
    r1.value = "";
    if (check()) {
        var d = parseFloat(n1.value) / parseFloat(n2.value);
        r1.value = d.toString();
    }
}
function reset() {
    n1.value = "";
    n2.value = "";
    r1.value = "";
}
//# sourceMappingURL=app.js.map
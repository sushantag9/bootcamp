[IPv4 Addressing](http://vlabs.iitb.ac.in/vlabs-dev/labs/computer_network/labs/exp4/index.php)
======

![ipv4](ip.png)



**Aim**
---------
-To give IP Address of different classes in given Network id.

-The goal of the experiment is to make user learn the concept of IP Addressing and let them manually set up IP address in the given netork and check for the same.

**Theory**
---------
IP addresses enable computers to communicate by providing unique identifiers for the computer itself and for the network over which it is located. An IP address is a 32 bit value that contains a network identifier(net -id) and a host identifier (host-id).
The network administrators need to assign IP addresses to the system on their network. This address needs to be a unique one. All the computers on a particular subnet will have the same network identifier but different host identifiers. The Internet Assigned Numbers Authority (IANA) assigns network identifiers to avoid any duplication of addresses.
Host Identifier Network Identifier 32 bits
The 32 bit IPv4 address is grouped into groups of eight bits, separated by dots. Each 8 bit group is then converted into its equivalent binary number. Thus each octet (8bit) can take value from 0 to 255. The IPv4 in the dotted decimal notation can range from 0.0.0.0 to 255.255.255.255.
IPv4 Address are classified into **5 types** as follows:
1. Class A     
2. Class B
3. Class C     
4. Class D
5. Class E

### Class A

The first bit of the first octet is always set to 0 (zero). Thus the first octet ranges from 1-127 i.e. 00000000 - 01111111

1-127 Class A addresses only include IP starting from 1.x.x.x to 126.x.x.x only. The IP range 127.x.x.x is reserved for loopback IP addresses. The default subnet mask for class Class A IP address is 255.0.0.0 which implies that Class A addressing can have 126 networks and 167777214 hosts. Class A IP address format is thus : 
**0NNNNNNN.HHHHHHHH.HHHHHHHH.HHHHHHHH**

### Class B

An IP address which belongs to class B has the first two bits in the first octet set to 10, i.e. 10000000 - 10111111

128 - 191 Class B IP Addresses range from 128.0.x.x to 191.255.x.x. The default subnet mask for Class B is 255.255.x.x. Class B has 16384 Network addresses and 65534 Host addresses. Class B IP addresses format is: 
**10NNNNNN.NNNNNNNN.HHHHHHHH.HHHHHHHH**

### Class C

The first octet of Class C IP address has its first 3 bits set to 110,that is: 11000000 - 11011111

192 - 223 Class C IP addresses range from 192.0.0.x to 223.255.255.x. The default subnet mask for Class C is 255.255.255.x. Class C gives 2097152 Network addresses and 254 Host addresses. Class C IP address format is : 
**110NNNNN.NNNNNNNN.NNNNNNNN.HHHHHHHH**



**Procedure**
--------
1. The aim is to Give IP Addresses to the PCs.
1. To perform the experiment follow the below steps.
1. A choice list would be given defining the Classes.
1. The user has to select the class in which they choose to give IP Addresses.
1. After that a Network ID would be given and the user has to enter the IP Addresses according to the Network ID.
1. Click on submit to test whether the IP address given to PCs make them into Network or not.

----
## Lab contributed by 
-**Maharashtra Institute of Technology, Aurangabad**
----


